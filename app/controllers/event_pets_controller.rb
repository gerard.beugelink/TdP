class EventPetsController < ApplicationController
  before_action :set_event_pet, only: [:show, :edit, :update, :destroy]

  # GET /event_pets
  # GET /event_pets.json
  def index
    @event_pets = EventPet.all.order(:date)
  end

  # GET /event_pets/1
  # GET /event_pets/1.json
  def show

  end

  # GET /event_pets/new
  def new
    @event_pet = EventPet.new
    # @pet_id = '1'
    @pet = Pet.find(params[:pet_id])
    # @vari = @pet.id
  end

  # GET /event_pets/1/edit
  def edit
  end

  # POST /event_pets
  # POST /event_pets.json
  def create
    
    @event_pet = EventPet.new(event_pet_params)

    respond_to do |format|
      if @event_pet.save
        format.html { redirect_to @event_pet.pet, notice: 'Event pet was successfully created.' }
        format.json { render :show, status: :created, location: @event_pet }
      else
        format.html { render :new }
        format.json { render json: @event_pet.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /event_pets/1
  # PATCH/PUT /event_pets/1.json
  def update
    respond_to do |format|
      if @event_pet.update(event_pet_params)
        format.html { redirect_to @event_pet.pet, notice: 'Event pet was successfully updated.' }
        format.json { render :show, status: :ok, location: @event_pet }
      else
        format.html { render :edit }
        format.json { render json: @event_pet.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /event_pets/1
  # DELETE /event_pets/1.json
  def destroy
    @event_pet.destroy
    respond_to do |format|
      format.html { redirect_to @event_pet.pet, notice: 'Event pet was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event_pet
      @event_pet = EventPet.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_pet_params
      params.require(:event_pet).permit(:event, :pet_id, :date)
    end
end
