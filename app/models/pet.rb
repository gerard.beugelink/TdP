class Pet < ActiveRecord::Base
  belongs_to :customer
  has_many :event_pets

  validates_presence_of(:name)
  validates_presence_of(:customer)
  
end
