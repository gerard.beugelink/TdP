class Customer < ActiveRecord::Base
	has_many :pets

	  validates_presence_of(:lastname)
end
