json.array!(@customers) do |customer|
  json.extract! customer, :id, :firstname, :lastname, :street, :housenumber, :postalcode, :city, :housephone, :mobilephone, :email
  json.url customer_url(customer, format: :json)
end
