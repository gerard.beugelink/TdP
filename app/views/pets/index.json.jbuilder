json.array!(@pets) do |pet|
  json.extract! pet, :id, :species, :name, :pelt, :birthday, :customer_id
  json.url pet_url(pet, format: :json)
end
