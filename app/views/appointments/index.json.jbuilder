json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :time, :comment, :pet_id
  json.url appointment_url(appointment, format: :json)
end
