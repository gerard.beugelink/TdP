json.array!(@event_pets) do |event_pet|
  json.extract! event_pet, :id, :event, :pet_id
  json.url event_pet_url(event_pet, format: :json)
end
