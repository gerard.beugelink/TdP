# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150201104215) do

  create_table "appointments", force: :cascade do |t|
    t.datetime "time"
    t.text     "comment"
    t.integer  "pet_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "appointments", ["pet_id"], name: "index_appointments_on_pet_id"

  create_table "customers", force: :cascade do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.string   "street"
    t.integer  "housenumber"
    t.string   "postalcode"
    t.string   "city"
    t.string   "housephone"
    t.string   "mobilephone"
    t.string   "email"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.boolean  "active"
  end

  create_table "event_pets", force: :cascade do |t|
    t.string   "event"
    t.integer  "pet_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date     "date"
  end

  add_index "event_pets", ["pet_id"], name: "index_event_pets_on_pet_id"

  create_table "pets", force: :cascade do |t|
    t.string   "species"
    t.string   "name"
    t.string   "pelt"
    t.date     "birthday"
    t.integer  "customer_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "pets", ["customer_id"], name: "index_pets_on_customer_id"

end
