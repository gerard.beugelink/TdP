class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.datetime :time
      t.text :comment
      t.references :pet, index: true

      t.timestamps null: false
    end
    add_foreign_key :appointments, :pets
  end
end
