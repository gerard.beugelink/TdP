class CreateEventPets < ActiveRecord::Migration
  def change
    create_table :event_pets do |t|
      t.string :event
      t.references :pet, index: true

      t.timestamps null: false
    end
    add_foreign_key :event_pets, :pets
  end
end
