class CreatePets < ActiveRecord::Migration
  def change
    create_table :pets do |t|
      t.string :species
      t.string :name
      t.string :pelt
      t.date :birthday
      t.references :customer, index: true

      t.timestamps null: false
    end
    add_foreign_key :pets, :customers
  end
end
