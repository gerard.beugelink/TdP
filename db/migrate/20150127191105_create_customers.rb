class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :firstname
      t.string :lastname
      t.string :street
      t.integer :housenumber
      t.string :postalcode
      t.string :city
      t.string :housephone
      t.string :mobilephone
      t.string :email

      t.timestamps null: false
    end
  end
end
