Customer.create!([
  {firstname: "Jannie", lastname: "Klaasen", street: "Hemonystraat", housenumber: 34, postalcode: "2837 GL", city: "Zutphen", housephone: "0575 123 456", mobilephone: "06 1234 5678", email: "mijn@email.com"},
  {firstname: "Bart", lastname: "Heinen", street: "Lange laan", housenumber: 423, postalcode: "4235 FF", city: "Deventer", housephone: "0570 543 848", mobilephone: "06 4325 5644", email: "ook@mail.com"}
])
Pet.create!([
  {species: "Berner Sennen", name: "Joep", pelt: "Effileren", birthday: "2013-06-15", customer_id: 2},
  {species: "Herder - duits", name: "Arko", pelt: "Knippen", birthday: "2015-01-28", customer_id: 1},
  {species: "Teckel - ruwhaar", name: "Jaja", pelt: "Plukken", birthday: "2015-01-29", customer_id: 2}
])
