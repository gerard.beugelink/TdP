require 'test_helper'

class EventPetsControllerTest < ActionController::TestCase
  setup do
    @event_pet = event_pets(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:event_pets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create event_pet" do
    assert_difference('EventPet.count') do
      post :create, event_pet: { event: @event_pet.event, pet_id: @event_pet.pet_id }
    end

    assert_redirected_to event_pet_path(assigns(:event_pet))
  end

  test "should show event_pet" do
    get :show, id: @event_pet
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @event_pet
    assert_response :success
  end

  test "should update event_pet" do
    patch :update, id: @event_pet, event_pet: { event: @event_pet.event, pet_id: @event_pet.pet_id }
    assert_redirected_to event_pet_path(assigns(:event_pet))
  end

  test "should destroy event_pet" do
    assert_difference('EventPet.count', -1) do
      delete :destroy, id: @event_pet
    end

    assert_redirected_to event_pets_path
  end
end
